var socket;
function connect(){
    
    socket = new WebSocket('ws://localhost:9876');
    socket.onmessage = function(event){
        if(event.data === "refresh"){
            location.reload();
        }
    }
    socket.onclose = function(e){
        console.log(e);
        setTimeout(connect,5000);
    }
}
    
connect();

function reload(){
    socket.send("refresh");
}
