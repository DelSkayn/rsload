use std::panic::{self, PanicInfo};
use std::io::Write;
use std::env;

use log::{self, LogLevel, SetLoggerError, LogLevelFilter, LogMetadata, Log, LogRecord};
extern crate backtrace;

extern crate time;
// use std::panic::{self,PanicInfo};


/// the basic logger.
/// Logs to a file specified by `LOG_FILE_PATH`;
/// TODO: Add option to change log file.
pub struct Logger {
    level: LogLevel,
}

impl Log for Logger {
    fn enabled(&self, meta: &LogMetadata) -> bool {
        meta.level() <= self.level
    }

    fn log(&self, record: &LogRecord) {
        if self.enabled(record.metadata()) {
            let test = format!("[{}]{}: {}\n",
                               record.level(),
                               time::now().strftime("%X").unwrap(),
                               record.args());
            print!("{}", test);
        }
    }
}

impl Logger {
    /// Initializes the logger
    /// Called once at the start of the engine.
    pub fn init() -> Result<(), SetLoggerError> {
        let res = log::set_logger(|max_log_level| {
            let level_string: String = env::var("LOG_LEVEL").unwrap_or("debug".to_string());
            println!("Logging at level: {}", level_string);
            let level = match level_string.as_str() {
                "debug" => LogLevel::Debug,
                "error" => LogLevel::Error,
                "warn" => LogLevel::Warn,
                "info" => LogLevel::Info,
                "trace" => LogLevel::Trace,
                _ => unreachable!(),
            };
            if cfg!(debug_assertions) {
                max_log_level.set(LogLevelFilter::Trace);
            } else {
                max_log_level.set(LogLevelFilter::Info);
            }
            Box::new(Logger { level: level })
        });
        panic::set_hook(Box::new(log_panic));
        res
    }
}

/// logs a panic and the stacktrace.
fn log_panic(info: &PanicInfo) {
    if let Some(x) = info.location() {
        error!("Panic in file \"{}\" at line \"{}\"!", x.file(), x.line());
    } else {
        error!("Panic from unkown location!");
    };

    if let Some(x) = info.payload().downcast_ref::<&'static str>() {
        error!("Payload: {}", x);
    } else if let Some(x) = info.payload().downcast_ref::<String>() {
        error!("Payload: {}", x);
    }

    let mut backtrace = Vec::<u8>::new();
    backtrace::trace(|frame| {
        let ip = frame.ip();
        backtrace::resolve(ip, |sym| {

            let name = sym.name()
                .map_or("! Unknown !", |x| x.as_str().unwrap_or("! Error !"));

            let line = sym.lineno()
                .map_or(-1, |x| x as i64);

            let filename = sym.filename()
                .map_or("! Unknown !", |x| x.to_str().unwrap_or("! Error !"));

            write!(&mut backtrace,
                   "[\x1b[31mbacktrace\x1b[0m] --> {}\n            file {}:{}\n\n",
                   name,
                   filename,
                   line)
                .is_ok();
        });
        true
    });
    error!("         ##Backtrace##\n{}",
           String::from_utf8(backtrace).unwrap_or("Error during backtrace generation".to_string()));
}
