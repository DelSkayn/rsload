use websocket::{ClientBuilder,OwnedMessage};
use clap;

pub fn close(matches: &clap::ArgMatches) {
    let ip = matches.value_of("ip").unwrap_or("localhost");
    let port = matches.value_of("port").unwrap_or("9876");
    let mut full_address = "ws://".to_string();
    full_address.push_str(ip);
    full_address.push(':');
    full_address.push_str(port);
    info!("Connecting to server on {}",full_address);

    let mut client = ClientBuilder::new(&full_address)
        .unwrap()
        .connect_insecure()
        .unwrap();

    client.send_message(&OwnedMessage::Text("close".to_string())).unwrap();
}
