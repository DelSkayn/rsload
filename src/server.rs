
use futures::{self,Future,Sink,Stream};
use tokio_core::reactor::{Handle,Core};

use websocket::async::Server;
use websocket::server::InvalidConnection;
use websocket::message::{Message,OwnedMessage};

use std::rc::Rc;
use std::cell::RefCell;
use std::fmt::Debug;
use std::collections::HashMap;
use std::process;

extern crate nix;
use log;

use clap;

pub fn server(matches: &clap::ArgMatches) {
    let ip = matches.value_of("ip").unwrap_or("localhost");
    let port = matches.value_of("port").unwrap_or("9876");

    if !matches.is_present("deamon"){
        info!("Forking");
        match nix::unistd::fork(){
            Ok(nix::unistd::ForkResult::Parent{ child: _ }) => {
                return;
            }
            Ok(nix::unistd::ForkResult::Child) => {}
            _ => {
                error!("Could not fork!");
                return;
            }
        };
        match nix::unistd::fork(){
            Ok(nix::unistd::ForkResult::Parent{ child: _ }) => {
                info!("Forking finished, shutdown logger");
                return;
            }
            Ok(nix::unistd::ForkResult::Child) => {}
            _ => {
                error!("Could not fork!");
                return;
            }
        };
        log::shutdown_logger().unwrap();
    }
    let mut full_address = ip.to_string();
    full_address.push(':');
    full_address.push_str(port);
    info!("Starting server on {}",full_address);

    let mut core = Core::new().unwrap();
    let handle = Rc::new(core.handle());

    let connections = Rc::new(RefCell::new(HashMap::new()));
    let connections_clone = connections.clone();

    let server = Server::bind(full_address,&handle).unwrap();
    let f = server.incoming()
        .map_err(|InvalidConnection{ error, ..}| error)
        .for_each(move |(upgrade, addr)|{
            info!("Incomming connection from {}",addr);

            let connections = connections_clone.clone();
            let handle_clone = handle.clone();
            
            let s = upgrade
                .accept()
                .and_then(move |(s,_)|{
                    let (write, reader) = s.split();

                    let (tx, rx) = futures::sync::mpsc::unbounded();
                    connections.borrow_mut().insert(addr,tx);

                    let connections_inner = connections.clone();
                    let connections_inners = connections.clone();


                    let reader = reader 
                        .take_while(|m| {
                            debug!("Recieved message :{:#?}",m);
                            Ok(!m.is_close())
                        })
                        .map(move |m|{

                            let conns = connections_inner.borrow_mut();

                            match m {
                                OwnedMessage::Ping(p) => {
                                    conns.get(&addr).unwrap().send(OwnedMessage::Pong(p)).unwrap();
                                }
                                OwnedMessage::Text(x) => {
                                    if x == "close"{
                                        process::exit(0);
                                    }
                                    for (i,v) in conns.iter() {
                                        if *i != addr {
                                            v.send(OwnedMessage::Text(x.clone())).unwrap();
                                        }
                                    }
                                }
                                _ => ()
                            }
                        })
                        .collect()
                        .map_err(|_| ())
                        .and_then(move |_|{
                            info!("Closing connection");
                            let mut borrow = connections_inners.borrow_mut();
                            (&borrow[&addr]).send(OwnedMessage::Close(None)).unwrap();
                            borrow.remove(&addr);
                            Ok(())
                        });

                    let writer = rx.fold(write, |writer,msg| {
                        writer.send(msg).map_err(|_|())
                    });

                    let connection = reader.map(|_| ()).select(writer.map(|_|()));
                    spawn_future(connection,"Client status",&handle_clone);
                    Ok(())
                });
            spawn_future(s,"Server status",&handle);
            Ok(())
        });

    core.run(f).unwrap();
}

fn spawn_future<F, I, E>(f: F, desc: &'static str, handle: &Handle)
where F: Future<Item = I, Error = E> + 'static,
{
    handle.spawn(f.map_err(move |_| error!("{}: ", desc))
                 .map(move |_| debug!("{}: Finished.", desc)));
}
