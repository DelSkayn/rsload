#![allow(unused_imports)]
#![allow(dead_code)]

extern crate websocket; extern crate clap;
#[macro_use]
extern crate log;
extern crate tokio_core;
extern crate futures;


use clap::{Arg, App, SubCommand,AppSettings};


mod server;
mod reload;
mod close;
mod logger;

use std::{thread,time};

fn main(){
    logger::Logger::init().unwrap();
    let app = App::new("Site reload command tool")
        .version(env!("CARGO_PKG_VERSION"))
        .author("Mees Delzenne")
        .about("This is a tool for automaticly reloading sites on command line instruction")
        .setting(AppSettings::SubcommandRequiredElseHelp)
        .global_setting(AppSettings::ColorAuto)
        .arg(Arg::with_name("delay")
             .help("Delay the execution of a command")
             .short("d")
             .value_name("TIME")
             .takes_value(true))
        .subcommand(SubCommand::with_name("run")
                    .about("Start the server used to connect commicate with the web page")
                    .arg(Arg::with_name("ip")
                         .help("The ip address the server listens to. By default localhost")
                         .short("i")
                         .value_name("IP")
                         .long("ip"))
                    .arg(Arg::with_name("port")
                         .help("The port server listens to. By default 9876")
                         .short("p")
                         .value_name("PORT")
                         .long("port"))
                    .arg(Arg::with_name("no deamon")
                         .short("d")
                         .help("Dont run the server as a deamon")))
        .subcommand(SubCommand::with_name("reload")
                    .about("Force the pages to reload")
                    .arg(Arg::with_name("IP")
                         .help("The ip address the deamon listens to. By default localhost")
                         .short("i")
                         .long("ip"))
                    .arg(Arg::with_name("PORT")
                         .help("The port deamon listens to. By default 9876")
                         .short("p")
                         .long("port")))
        .subcommand(SubCommand::with_name("close")
                    .about("Close the server deamon")
                    .arg(Arg::with_name("ip")
                         .help("The ip address the server listens to. By default localhost")
                         .short("i")
                         .value_name("IP")
                         .long("ip"))
                    .arg(Arg::with_name("port")
                         .help("The port server listens to. By default 9876")
                         .short("p")
                         .value_name("PORT")
                         .long("port"))).get_matches();

    if let Some(x) = app.value_of("delay"){
        let time = x.parse().unwrap_or(0);
        info!("Delaying for {} secs",time);
        thread::sleep(time::Duration::from_millis(time));
    }

    if let Some(matches) = app.subcommand_matches("run"){
        server::server(matches);
    }

    if let Some(matches) = app.subcommand_matches("reload"){
        reload::reload(matches);
    }

    if let Some(matches) = app.subcommand_matches("close"){
        close::close(matches);
    }
}


